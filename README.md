# dusbank

Sandbox project for new experimental Dusbank.

## How to update the site ?

1. Make changes
2. Commit & Push
3. Wait for pipeline to finish
4. You can see you change here : https://tony-engineering.gitlab.io/dusbank